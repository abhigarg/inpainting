#include "inpainting.hpp"
using namespace std;
using namespace cv;

bool debuging = false;

int main(int argc, char** argv)
{
	//output log file
	ofstream log;
	log.open("log.txt");

	string path_img;
	string path_mask;
	string output_path;
	int sz;
	int sf = 10; // search factor

	if (argc < 5)
	{
		if (argc == 4)
		{
			char* arg_p;
			char* arg_q;
			char* arg_r;
			long converted1 = strtol(argv[1], &arg_p, 10);
			long converted2 = strtol(argv[2], &arg_q, 10);
			long converted3 = strtol(argv[3], &arg_r, 10);
			if (*arg_p && *arg_q && *arg_r)
			{
				// Arguments are not a number. Assuming kernel size ommitted
				path_img = argv[1];
				path_mask = argv[2];
				output_path = argv[3];
				sz = 7;
				sf = 10;
				log << "Kernel size not part of argument list. Set to default of " << sz << "\n\n";
			}
			else {
				// One of the arguments is a number. Assuming missing information
				cout << "Not enough input arguments\n Usage: inpainting.exe <path_to_image> ,path_to_mask> <result_image_path> [kernel_size] \n\n";
				log << "Not enough input arguments\n Usage: inpainting.exe <path_to_image> ,path_to_mask> <result_image_path> [kernel_size] \n\n";
				return -1;
			}
		}
		else
		{
			cout << "Not enough input arguments\n Usage: inpainting.exe <path_to_image> ,path_to_mask> <result_image_path> [kernel_size] [debug_mode] \n\n";
			log << "Not enough input arguments\n Usage: inpainting.exe <path_to_image> ,path_to_mask> <result_image_path> [kernel_size] \n\n";
			return -1;
			/*
			cout << "Not enough input arguments\n **-Using Defaults-**\n\n";

			path_img = "C:\\Users\\Conrad\\Desktop\\21_July\\P_imageH.jpg";
			path_mask = "C:\\Users\\Conrad\\Desktop\\21_July\\P_maskH.bmp";
			sz = 7;
			sf = 15;
			output_path = "C:\\Users\\Conrad\\Desktop\\21_July\\PH_output_K7_S15_.jpg";
			cout << "Image - " << path_img << "\n";
			cout << "Mask  - " << path_mask << "\n";
			cout << "Psi size = " << sz << "\n";
			cout << "Output Image - " << output_path << "\n\n";
			*/
		}
	}
	else
	{
		path_img = argv[1];
		path_mask = argv[2];
		output_path = argv[3];
		sz = atoi(argv[4]);
		sf = 10;
	}
	if (argc == 6 || argc == 7)
	{
		if (argc == 7)
			sf = atoi(argv[6]);

		char* arg_s;
		long converted = strtol(argv[5], &arg_s, 10);
		if (*arg_s)
		{
			// Argument is not a number.
			debuging = false;
			log << "Debuging Flag = false. Suppressing console output\n\n";
		}
		else
		{
			if (atoi(argv[5]) >= 1)
			{
				debuging = true;
				log << "Debuging Flag = true. Enabling console output\n\n";
				cout << "Debuging Flag = true. Enabling console output\n\n";
			}
		}
	}
	
	cv::Mat img_rgb = imread(path_img, CV_LOAD_IMAGE_COLOR);
	cv::Mat mask = imread(path_mask, 0);

	if (!img_rgb.data)                              // Check for invalid input
	{
		log << "Could not open or find the input image" << endl;
		if (debuging == true)
			cout << "Could not open or find the input image" << endl;
		log.close();
		return -2;
	}
	else
	{
		log << "Image loaded : " << path_img << endl;
	}

	if (!mask.data)
	{
		log << "Could not open or find the mask image" << endl;
		if (debuging == true)
			cout << "Could not open or find the mask image: " << path_mask << endl;
		log.close();
		return -3;
	}
	else
	{
		log << "Mask loaded : " << path_mask << endl;
	}

	log << "Patch kernel size set at " << sz << endl;
	log << "Search factor set at " << sf << endl;


	// start clock
	clock_t t = clock();

	//call inpaint
	Mat out_img;
	apply_inpaint(img_rgb, mask, sz, sf, debuging, out_img);

	t = clock() - t;
	imwrite(output_path, out_img);
	if (debuging == true)
	{
		cout << "\n\n**********************************" << endl;
		cout << "Time taken in secs: " << t / (double)CLOCKS_PER_SEC << endl;
		cout << "**********************************\n\n" << endl;
		imshow("output image", out_img);
		waitKey(0);
	}

	log << "Resulting image output : " << output_path << endl;
	log << "Execution in " << t / (double)CLOCKS_PER_SEC << " seconds." << endl;
	log << "*********************************************************************************" << endl;
	log << "*********************************************************************************" << endl << endl << endl;
	log.close();

}